﻿namespace Models.Enums
{
    public enum FieldGroupId
    {
        Episode,
        MedisinskKode,
        SnmpKode,
        Særkode,
        DrgBeregningResultat,
    }
}
