﻿namespace Models.Enums
{
    public enum LogicalOperator
    {
        And,
        Or
    }
}