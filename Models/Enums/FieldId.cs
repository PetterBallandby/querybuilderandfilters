﻿namespace Models.Enums
{
    public enum FieldId
    {
        Hovedtilstand,
        OmsorgNivå,
        Debitor,
        MedisinskKodeverk,
        Prosedyrekode,
        NcmpKodeVerdi,
        NcspKodeVerdi,
        KodeVerdi,
        EpisodeOid,
        AntallEpisoder,
        InnDatoTid,
        AntallProsedyrekoder
    }
}