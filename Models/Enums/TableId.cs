﻿namespace Models.Enums
{
    public enum TableId
    {
        Episode,
        EpisodeResultat,
        DrgBeregningResultat,
        Tjeneste,
        MedisinskKode,
    }
}