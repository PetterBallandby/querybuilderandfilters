namespace Models.Enums
{
    public enum FieldOperator
    {
        LessThan,
        LessThanOrEqual,
        GreaterThan,
        GreaterThanOrEqual,
        Equals,
        StartsWith,
        EndsWith,
        Contains,
        Between,
    }
}