﻿namespace Models.Enums
{
    public enum ColumnId
    {
        Oid,
        EpisodeOid,
        OmsorgNivå,
        Debitor,
        MedisinskKodeverk,
        KodeVerdi,
        InnDatoTid,
        Hovedtilstand
    }
}