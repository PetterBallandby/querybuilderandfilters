﻿using Models.Enums;

namespace Models.QueryBuilders
{
    public class TableColumn
    {
        public TableColumn(TableId tableId, ColumnId columnId)
        {
            TableId = tableId;
            ColumnId = columnId;
        }

        public TableId TableId { get; }
        public ColumnId ColumnId { get; }
    }
}
