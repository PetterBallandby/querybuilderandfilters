﻿using Models.Enums;

namespace Models.QueryBuilders
{
    public class FieldInfo
    {
        public TableColumn TableColumn { get; set; }
        public DbFunc? DbFunc { get; set; }
    }
}