﻿using System;
using Models.Enums;
using System.Collections.Generic;
using Models.Filters;
using Models.Interfaces;

namespace Models.QueryBuilders
{
    public abstract class QueryBuilder : IQueryBuilder
    {
        private Filter _filter;

        #region Constructors

        protected QueryBuilder(TableId baseTableId)
        {
            BaseTableId = baseTableId;
        }

        #endregion

        #region Properties

        public TableId BaseTableId { get; }

        private bool HasDbFunc { get; set; }

        public IList<FieldId> Fields { get; set; }

        public Filter Filter
        {
            get { return _filter; }
            set
            {
                _filter = value; 
            }
        }

        public string Sql { get; private set; }

        private IList<string> Selects { get; } = new List<string>();
        private IList<string> Froms { get; } = new List<string>();
        private IList<string> Wheres { get; } = new List<string>();
        private IList<string> GroupBys { get; } = new List<string>();

        #endregion

        #region Methods

        public void GenerateSql()
        {
            foreach (var fieldId in Fields)
                HandleField(fieldId);

            HandleFilter(Filter);

            BuildGroupBys();

            BuildSql();
        }

        private void BuildGroupBys()
        {
            if (!HasDbFunc)
                return;

            foreach (var fieldId in Fields)
            {
                var fieldInfo = FieldInfos[fieldId];
                if (!fieldInfo.DbFunc.HasValue)
                    GroupBys.Add(GetFieldSelect(fieldId));
            }
        }

        private void HandleFilter(Filter filter)
        {
            if (filter == null)
                return;

            filter.QueryBuilder = this;

            var combination = filter as CombinationFilter;
            if (combination != null)
                HandleCombinationFilter(combination);
            else
                HandleFieldFilter(filter as FieldFilter);
        }

        private void HandleFieldFilter(FieldFilter fieldFilter)
        {
            if (fieldFilter is CountFilter)
                HandleCountFilter(fieldFilter as CountFilter);
        }

        private void HandleCountFilter(CountFilter countFilter)
        {
            countFilter.BaseTableAlias = TableInfos[BaseTableId].Alias;
        }

        private void HandleCombinationFilter(CombinationFilter combinationFilter)
        {
            foreach (var filter in combinationFilter.Filters)
            {
                var fieldFilter = filter as FieldFilter;
                if (fieldFilter != null)
                {
                    var sql = filter.GetSql();
                    Wheres.Add(sql);
                }
            }
        }

        private void BuildSql()
        {
            AddBaseTable();

            var select = $"SELECT {string.Join(", ", Selects)} ";
            var from = $"FROM {string.Join("\n", Froms)} ";
            var where = Wheres.Count > 0 ? $"WHERE {string.Join(" AND\n", Wheres)} " : null;
            var groupBy = GroupBys.Count > 0 ? $"GROUP BY {string.Join(", ", GroupBys)}" : null;

            Sql = string.Join("\n", select, from, where, groupBy);
        }

        private void AddBaseTable()
        {
            var tableInfo = TableInfos[BaseTableId];
            Froms.Add($"{tableInfo.Schema}.{BaseTableId} {tableInfo.Alias}");
        }

        private void HandleField(FieldId fieldId)
        {
            var fieldInfo = FieldInfos[fieldId];
            if (fieldInfo.TableColumn.TableId == BaseTableId)
            {
                var select = fieldInfo.DbFunc.HasValue ? GetFuncSelect(fieldInfo.DbFunc.Value, fieldId) : GetFieldSelect(fieldId);
                Selects.Add(select);
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        private string GetFuncSelect(DbFunc dbFunc, FieldId fieldId)
        {
            HasDbFunc = true;

            switch (dbFunc)
                {
                    case DbFunc.Count:
                        return $"COUNT (DISTINCT {GetFieldSelect(fieldId)})";
                    case DbFunc.Sum:
                        return $"SUM ({GetFieldSelect(fieldId)})";
                    default:
                        throw new ArgumentOutOfRangeException(nameof(dbFunc), dbFunc, null);
                }
        }

        private string GetFieldSelect(FieldId fieldId)
        {
            var fieldInfo = FieldInfos[fieldId];
            var tableInfo = TableInfos[fieldInfo.TableColumn.TableId];

            return $"{tableInfo.Alias}.{fieldInfo.TableColumn.ColumnId}";
        }

        #endregion
    }
}