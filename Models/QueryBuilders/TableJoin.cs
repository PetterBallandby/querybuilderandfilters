﻿using Models.Filters;

namespace Models.QueryBuilders
{
    public class TableJoin
    {
        public TableColumn Source { get; set; }
        public TableColumn Target { get; set; }
        public Filter Filter { get; set; }
    }
}