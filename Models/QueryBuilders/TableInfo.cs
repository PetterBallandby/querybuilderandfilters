﻿namespace Models.QueryBuilders
{
    public class TableInfo
    {
        public TableInfo(string alias)
        {
            Alias = alias;
        }

        public string Schema { get; } = "dbo";
        public string Alias { get; }
    }
}