﻿using System.Collections.Generic;
using Models.Enums;

namespace Models.QueryBuilders
{
    public class EpisodeQueryBuilder : QueryBuilder
    {
        #region Constructors

        public EpisodeQueryBuilder() : base(TableId.Episode)
        {
        }

        #endregion

        #region Properties

        public override IDictionary<TableId, IList<TableJoin>> TableJoins { get; } = new Dictionary<TableId, IList<TableJoin>>
        {
            {
                TableId.DrgBeregningResultat, new List<TableJoin>
                {
                    new TableJoin
                    {
                        Source = new TableColumn(TableId.Episode, ColumnId.Oid),
                        Target = new TableColumn(TableId.EpisodeResultat, ColumnId.EpisodeOid)
                    }
                }
            },
            {
                TableId.MedisinskKode, new List<TableJoin>
                {
                    new TableJoin
                    {
                        Source = new TableColumn(TableId.Episode, ColumnId.Oid),
                        Target = new TableColumn(TableId.MedisinskKode, ColumnId.EpisodeOid)
                    }
                }
            }
        };

        public override IDictionary<FieldId, FieldInfo> FieldInfos { get; } = new Dictionary<FieldId, FieldInfo>
        {
            {
                FieldId.AntallEpisoder, new FieldInfo
                {
                    TableColumn = new TableColumn(TableId.Episode, ColumnId.Oid),
                    DbFunc = DbFunc.Count
                }
            },
            {
                FieldId.OmsorgNivå, new FieldInfo
                {
                    TableColumn = new TableColumn(TableId.Episode, ColumnId.OmsorgNivå)
                }
            },
            {
                FieldId.Debitor, new FieldInfo
                {
                    TableColumn = new TableColumn(TableId.Episode, ColumnId.Debitor)
                }
            },
            {
                FieldId.AntallProsedyrekoder,
                new FieldInfo
                {
                    TableColumn = new TableColumn(TableId.MedisinskKode, ColumnId.Oid),
                    DbFunc = DbFunc.Count,
                }
            },
            {
                FieldId.InnDatoTid,
                new FieldInfo
                {
                    TableColumn = new TableColumn(TableId.Episode, ColumnId.InnDatoTid),
                }
            },
            //    {FieldId.Debitor, new TableColumn(TableId.Episode, ColumnId.Debitor)},
            //    {FieldId.MedisinskKodeverk, new TableColumn(TableId.MedisinskKode, ColumnId.MedisinskKodeverk)},
            //    {FieldId.KodeVerdi, new TableColumn(TableId.MedisinskKode, ColumnId.KodeVerdi)},
        };

        #endregion
    }
}
