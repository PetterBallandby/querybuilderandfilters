﻿using System;
using System.Collections.Generic;
using Models.Enums;
using Models.QueryBuilders;

namespace Models.Filters
{
    public class CountFilter : FieldFilter<int>
    {
        #region Constructors

        public CountFilter(FieldId fieldId) : base(fieldId)
        {
        }

        #endregion

        public IList<TableJoin> Joins { get; set; }
        public string BaseTableAlias { get; set; }

        #region FieldFilter<int>

        public override string Description
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override string GetSql()
        {
            string sqlOperator;
            switch (FieldOperator)
            {
                case FieldOperator.LessThan:
                    sqlOperator = "<";
                    break;
                case FieldOperator.LessThanOrEqual:
                    sqlOperator = "<=";
                    break;
                case FieldOperator.GreaterThan:
                    sqlOperator = ">";
                    break;
                case FieldOperator.GreaterThanOrEqual:
                    sqlOperator = ">=";
                    break;
                case FieldOperator.Equals:
                    sqlOperator = "=";
                    break;
                case FieldOperator.Between:
                    sqlOperator = "<";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            var fieldSelect = GetFieldSelect();
            fieldSelect += $"FROM dbo.MedisinskKode mk WHERE mk.EpisodeOid = {TableAlias}.{ColumnId}";
            return $"({fieldSelect}) {sqlOperator} {FilterValues[0]}";
        }

        #endregion

        #region Methods

        private string GetFieldSelect()
        {
            return "SELECT COUNT(DISTINCT mk.Oid)";
        }

        #endregion
    }
}