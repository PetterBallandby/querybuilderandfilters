﻿using System.Collections.Generic;
using Models.Enums;
using Models.Interfaces;

namespace Models.Filters
{
    public class CombinationFilter : Filter
    {
        private IQueryBuilder _queryBuilder;
        public LogicalOperator LogicalOperator { get; set; }
        public IList<Filter> Filters { get; set; }

        public override IQueryBuilder QueryBuilder
        {
            get { return _queryBuilder; }
            set
            {
                _queryBuilder = value;
                foreach (var filter in Filters)
                    filter.QueryBuilder = value;
            }
        }

        public override string Description
        {
            get
            {
                var descriptions = new List<string>();
                foreach (var filter in Filters)
                    descriptions.Add(filter.Description);

                var concatenatedString = string.Join($" {LogicalOperator} ", descriptions);
                return $"({concatenatedString})";
            }
        }

        public override string GetSql()
        {
            var list = new List<string>();
            foreach (var filter in Filters)
                list.Add(filter.GetSql());

            var concatenatedString = string.Join($" {LogicalOperator} ", list);
            return $"({concatenatedString})";
        }
    }
}
