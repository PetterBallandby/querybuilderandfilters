﻿using System;
using System.Collections.Generic;
using Models.Enums;

namespace Models.Filters
{
    public class StringFilter : FieldFilter<string>
    {
        public StringFilter(FieldId fieldId) : base(fieldId)
        {
        }

        public override string Description
        {
            get
            {
                if (FilterValues.Count == 0)
                    return string.Empty;

                return $"{FieldOperator} ({string.Join(", ", FilterValues)})";
            }
        }

        public override string GetSql()
        {
            switch (FieldOperator)
            {
                case FieldOperator.Equals:
                    return $"{TableAlias}.{ColumnId} IN ({string.Join(", ", FilterValues)})";
                case FieldOperator.StartsWith:
                    return GetLikeList(false, true, FilterValues);
                case FieldOperator.EndsWith:
                    return GetLikeList(true, false, FilterValues);
                case FieldOperator.Contains:
                    return GetLikeList(true, true, FilterValues);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private string GetLikeList(bool usePrefixWildcard, bool usePostfixWildcard, IEnumerable<string> values)
        {
            var prefixWildcard = usePrefixWildcard ? "%" : string.Empty;
            var postfixWildcard = usePostfixWildcard ? "%" : string.Empty;

            var valueList = new List<string>();
            foreach (var value in values)
                valueList.Add($"{TableAlias}.{ColumnId} LIKE '{prefixWildcard}{value}{postfixWildcard}'");

            var sql = string.Join(" OR ", valueList);
            return valueList.Count > 1 ? $"({sql})" : sql;
        }
    }
}
