﻿using System;
using Models.Enums;

namespace Models.Filters
{
    public class DateTimeFilter : FieldFilter<DateTime>
    {
        public DateTimeFilter(FieldId fieldId) : base(fieldId)
        {
        }

        public override string Description { get; }
        public override string GetSql()
        {
            switch (FieldOperator)
            {
                case FieldOperator.Between:
                    return $"{TableAlias}.{ColumnId} BETWEEN '{FilterValues[0]}' AND '{FilterValues[1]}'";

                case FieldOperator.LessThan:
                case FieldOperator.LessThanOrEqual:
                case FieldOperator.GreaterThan:
                case FieldOperator.GreaterThanOrEqual:
                case FieldOperator.Equals:
                case FieldOperator.StartsWith:
                case FieldOperator.EndsWith:
                case FieldOperator.Contains:
                    throw new NotImplementedException();

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}