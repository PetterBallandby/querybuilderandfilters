﻿using System.Collections.Generic;
using Models.Enums;
using Models.QueryBuilders;

namespace Models.Filters
{
    public abstract class FieldFilter : Filter
    {
        protected FieldFilter(FieldId fieldId)
        {
            FieldId = fieldId;
        }

        public FieldId FieldId { get; }
        public FieldOperator FieldOperator { get; set; }

        public string TableAlias
        {
            get { return QueryBuilder.FieldTableInfoMap[FieldId].Alias; }
        }

        public ColumnId ColumnId
        {
            get { return QueryBuilder.FieldInfos[FieldId].TableColumn.ColumnId; }
        }

        public bool Invert { get; set; }

        public IList<TableJoin> TableJoins { get; set; }
    }

    public abstract class FieldFilter<T> : FieldFilter
    {
        protected FieldFilter(FieldId fieldId) : base(fieldId)
        {
        }

        public IList<T> FilterValues { get; } = new List<T>();
    }
}