﻿using Models.Interfaces;

namespace Models.Filters
{
    public abstract class Filter : IFilter
    {
        public abstract string Description { get; }

        public virtual IQueryBuilder QueryBuilder { get; set; }

        public abstract string GetSql();
    }
}