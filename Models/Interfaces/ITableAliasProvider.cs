﻿using Models.Enums;

namespace Models.Interfaces
{
    public interface ITableAliasProvider
    {
        string this[FieldId fieldId] { get; }
    }
}
