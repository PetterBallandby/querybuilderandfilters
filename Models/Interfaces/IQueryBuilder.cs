﻿using System.Collections.Generic;
using Models.Enums;
using Models.Filters;

namespace Models.Interfaces
{
    public interface IQueryBuilder
    {
        IList<FieldId> Fields { get; }
        Filter Filter { get; set; }
        string Sql { get; }
    }
}