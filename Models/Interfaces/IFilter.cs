﻿namespace Models.Interfaces
{
    public interface IFilter
    {
        string Description { get; }
        IQueryBuilder QueryBuilder { get; set; }

        string GetSql();
    }
}