﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Models.Enums;
using Models.Filters;
using Models.QueryBuilders;
using NUnit.Framework;

namespace ModelsUnitTests
{
    [TestFixture]
    public class EpisodeQueryBuilderUnitTests
    {
        [Test]
        [TestCaseSource(typeof(TestInput), nameof(TestInput.KunEpisodeFelter))]
        [TestCaseSource(typeof(TestInput), nameof(TestInput.AntallEpisoderFordeltPåNcmpKodeOgSærkode))]
        [TestCaseSource(typeof(TestInput), nameof(TestInput.AntallEpisoderMedProsedyrekoderFordeltPåOmsorgsnivå))]
        [TestCaseSource(typeof(TestInput), nameof(TestInput.AntallEpisoderUtenProsedyrekoderFordeltPåOmsorgsnivå))]
        [TestCaseSource(typeof(TestInput), nameof(TestInput.AntallEpisoderMedMinstToProsedyrekoderFordeltPåProsedyrekoder))]
        [TestCaseSource(typeof(TestInput), nameof(TestInput.AntallEpisoderMedNcmpkoderFordeltPåOmsorgsnivå))]
        [TestCaseSource(typeof(TestInput), nameof(TestInput.AntallEpisoderUtenNcmpkoderFordeltPåOmsorgsnivå))]
        [TestCaseSource(typeof(TestInput), nameof(TestInput.AntallEpisoderFordeltPåProsedyrekode))]
        [TestCaseSource(typeof(TestInput), nameof(TestInput.AntallEpisoderFordeltPåNcmpKode))]
        [TestCaseSource(typeof(TestInput), nameof(TestInput.AntallEpisoderMedAngittProsedyrekodeFordeltPåOmsorgsnivå))]
        [TestCaseSource(typeof(TestInput), nameof(TestInput.AntallEpisoderMedAngittNcmpKodeFordeltPåOmsorgNivåOgDebitor))]
        [TestCaseSource(typeof(TestInput), nameof(TestInput.AntallEpisoderMedAngittSærKodeFordeltPåSærkodeOgNcmpKode))]
        [TestCaseSource(typeof(TestInput), nameof(TestInput.AntallEpisoderMedAngittSærkodeFordeltPåOmsorgsnivå))]
        [TestCaseSource(typeof(TestInput), nameof(TestInput.AntallEpisoderMedAngittSærkodeFordeltPåOmsorgsnivåOgSærkode))]
        [TestCaseSource(typeof(TestInput), nameof(TestInput.AntallEpisoderMedAngittSærkodeOgAngittNcmpKodeFordeltPåOmsorgsnivåOgSærkode))]
        public void QueryBuilderReturnsExpectedSql(string caseName, IEnumerable<FieldId> fields, Filter filter, string expectedSql)
        {
            var queryBuilder = new EpisodeQueryBuilder
            {
                Fields = new List<FieldId>(fields),
                Filter =  filter,
            };

            queryBuilder.GenerateSql();

            Assert.That(Regex.Replace(queryBuilder.Sql, @"\s+", "").ToUpper(), Is.EqualTo(Regex.Replace(expectedSql, @"\s+", "").ToUpper()));
        }

        #region Class: TestInput

        private static class TestInput
        {
            private static readonly DateTime FraDatotid = new DateTime(2016, 1, 1);
            private static readonly DateTime TilDatotid = new DateTime(2016, 12, 31, 23, 59, 59);

            private const string MedisinskKodeverkSærkode = "S";
            private const string MedisinskKodeverkNcmp = "M";
            private const string Særkode = "1X";
            private const string NcmpKode = "WBG";

            public static IEnumerable KunEpisodeFelter
            {
                // ReSharper disable once UnusedMember.Local
                get
                {
                    yield return new TestCaseData(
                        nameof(KunEpisodeFelter),
                        new List<FieldId>{FieldId.OmsorgNivå, FieldId.Debitor},
                        null,
                        @"
SELECT e.Omsorgnivå, e.Debitor
FROM dbo.Episode e");
                }
            }

            public static IEnumerable AntallEpisoderMedAngittNcmpKodeFordeltPåOmsorgNivåOgDebitor
            {
                // ReSharper disable once UnusedMember.Local
                get
                {
                    yield return new TestCaseData(
                        nameof(AntallEpisoderMedAngittNcmpKodeFordeltPåOmsorgNivåOgDebitor),
                        new List<FieldId>{FieldId.AntallEpisoder, FieldId.OmsorgNivå, FieldId.Debitor},
                        new CombinationFilter
                        {
                            LogicalOperator = LogicalOperator.And,
                            Filters = new List<Filter>
                            {
                                new StringFilter(FieldId.MedisinskKodeverk)
                                {
                                    FieldOperator = FieldOperator.Equals,
                                    FilterValues = { "M"},
                                },
                                new StringFilter(FieldId.KodeVerdi)
                                {
                                    FieldOperator = FieldOperator.StartsWith, 
                                    FilterValues = { "WBG"},
                                },
                                new DateTimeFilter(FieldId.InnDatoTid)
                                {
                                    FieldOperator = FieldOperator.Between,
                                    FilterValues = { FraDatotid, TilDatotid},
                                }
                            }
                        },
                        $@"
SELECT 
    COUNT(e.Oid), 
    OmsorgNivå, 
    Debitor
FROM 
    Episode e
WHERE 
    EXISTS (SELECT 'x' FROM MedisinskKode mk WHERE mk.EpisodeOid=e.Oid AND mk.MedisinskKodeverk='{MedisinskKodeverkNcmp}' and mk.KodeVerdi like '{NcmpKode}%') AND
    e.InnDatoTid BETWEEN '{FraDatotid}' AND '{TilDatotid}'
GROUP BY OmsorgNivå, Debitor
");
                }
            }

            public static IEnumerable AntallEpisoderMedProsedyrekoderFordeltPåOmsorgsnivå
            {
                // ReSharper disable once UnusedMember.Local
                get
                {
                    yield return new TestCaseData(
                        nameof(AntallEpisoderMedProsedyrekoderFordeltPåOmsorgsnivå),
                        new List<FieldId>{FieldId.AntallEpisoder, FieldId.OmsorgNivå},
                        new CombinationFilter
                        {
                            LogicalOperator = LogicalOperator.And,
                            Filters = new List<Filter>
                            {
                                new CountFilter(FieldId.AntallProsedyrekoder)
                                {
                                    FieldOperator = FieldOperator.GreaterThan,
                                    FilterValues = { 0 },
                                },
                                new DateTimeFilter(FieldId.InnDatoTid)
                                {
                                    FieldOperator = FieldOperator.Between,
                                    FilterValues = { FraDatotid, TilDatotid},
                                }
                            }
                        },
                        $@"
SELECT
    COUNT (DISTINCT e.Oid),
    e.OmsorgNivå
FROM
    dbo.Episode e
WHERE
    (SELECT COUNT(DISTINCT mk.Oid) FROM dbo.MedisinskKode mk WHERE mk.EpisodeOid=e.Oid) > 0 AND
    e.InnDatoTid BETWEEN '{FraDatotid}' AND '{TilDatotid}'
GROUP BY
    e.OmsorgNivå
");
                }
            }

            public static IEnumerable AntallEpisoderUtenProsedyrekoderFordeltPåOmsorgsnivå
            {
                // ReSharper disable once UnusedMember.Local
                get
                {
                    yield return new TestCaseData(
                        nameof(AntallEpisoderUtenProsedyrekoderFordeltPåOmsorgsnivå),
                        new List<FieldId> {FieldId.AntallEpisoder, FieldId.OmsorgNivå},
                        new CombinationFilter
                        {
                            LogicalOperator = LogicalOperator.And,
                            Filters = new List<Filter>
                            {
                                new CountFilter(FieldId.AntallProsedyrekoder)
                                {
                                    FieldOperator = FieldOperator.Equals,
                                    FilterValues = {0},
                                },
                                new DateTimeFilter(FieldId.InnDatoTid)
                                {
                                    FieldOperator = FieldOperator.Between,
                                    FilterValues = {FraDatotid, TilDatotid},
                                }
                            }
                        },
                        $@"
SELECT 
    COUNT (DISTINCT e.Oid), 
    e.OmsorgNivå
FROM 
    dbo.Episode e
WHERE 
    (SELECT COUNT(DISTINCT mk.Oid) FROM dbo.MedisinskKode mk WHERE mk.EpisodeOid=e.Oid) = 0 AND
    e.InnDatoTid BETWEEN '{FraDatotid}' AND '{TilDatotid}'
GROUP BY e.OmsorgNivå");
                }
            }

            public static IEnumerable AntallEpisoderMedMinstToProsedyrekoderFordeltPåProsedyrekoder
            {
                // ReSharper disable once UnusedMember.Local
                get
                {
                    yield return new TestCaseData(
                        nameof(AntallEpisoderMedMinstToProsedyrekoderFordeltPåProsedyrekoder),
                        new List<FieldId> { FieldId.AntallEpisoder, FieldId.Prosedyrekode },
                        new CombinationFilter
                        {
                            LogicalOperator = LogicalOperator.And,
                            Filters = new List<Filter>
                            {
                                new CountFilter(FieldId.AntallProsedyrekoder)
                                {
                                    FieldOperator = FieldOperator.GreaterThanOrEqual,
                                    FilterValues = { 2 },
                                },
                                new DateTimeFilter(FieldId.InnDatoTid)
                                {
                                    FieldOperator = FieldOperator.Between,
                                    FilterValues = { FraDatotid, TilDatotid},
                                }
                            }
                        },
                        $@"
SELECT
    COUNT (DISTINCT e.Oid),
    mk.KodeVerdi
FROM
    dbo.Episode e,
    LEFT JOIN dbo.MedisinskKode
WHERE
    (SELECT COUNT(DISTINCT mk.Oid) FROM dbo.MedisinskKode mk WHERE mk.EpisodeOid=e.Oid) > 0 AND
    e.InnDatoTid BETWEEN '{FraDatotid}' AND '{TilDatotid}'
GROUP BY
    mk.KodeVerdi
");
                }
            }

            public static IEnumerable AntallEpisoderMedNcmpkoderFordeltPåOmsorgsnivå
            {
                // ReSharper disable once UnusedMember.Local
                get
                {
                    yield return new TestCaseData(
                        nameof(AntallEpisoderMedNcmpkoderFordeltPåOmsorgsnivå),
                        new List<FieldId>{FieldId.AntallEpisoder, FieldId.Prosedyrekode},
                        new CombinationFilter
                        {
                            LogicalOperator = LogicalOperator.And,
                            Filters = new List<Filter>
                            {
                                new CountFilter(FieldId.AntallProsedyrekoder)
                                {
                                    FieldOperator  = FieldOperator.Equals,
                                    FilterValues = { 0 }
                                },
                                new DateTimeFilter(FieldId.InnDatoTid)
                                {
                                    FieldOperator = FieldOperator.Between,
                                    FilterValues = { FraDatotid, TilDatotid},
                                }
                            }
                        },
                        $@"
SELECT 
    COUNT (DISTINCT (e.Oid)), 
    e.OmsorgNivå
FROM 
    dbo.Episode e
WHERE 
    EXISTS (SELECT 'x' FROM dbo.MedisinskKode mk WHERE mk.EpisodeOid=e.Oid AND mk.MedisinskKodeverk='{MedisinskKodeverkNcmp}')
    e.InnDatoTid BETWEEN '{FraDatotid}' AND '{TilDatotid}'
GROUP BY mk.KodeVerdi");
                }
            }

            public static IEnumerable AntallEpisoderUtenNcmpkoderFordeltPåOmsorgsnivå
            {
                // ReSharper disable once UnusedMember.Local
                get
                {
                    yield return new TestCaseData(
                        nameof(AntallEpisoderUtenNcmpkoderFordeltPåOmsorgsnivå),
                        new List<FieldId>{FieldId.AntallEpisoder, FieldId.Prosedyrekode},
                        new CombinationFilter
                        {
                            LogicalOperator = LogicalOperator.And,
                            Filters = new List<Filter>
                            {
                                new DateTimeFilter(FieldId.InnDatoTid)
                                {
                                    FieldOperator = FieldOperator.Between,
                                    FilterValues = { FraDatotid, TilDatotid},
                                }
                            }
                        },
                        $@"
SELECT 
    COUNT (DISTINCT (e.Oid)), 
    e.OmsorgNivå
FROM 
    dbo.Episode e
WHERE 
    NOT EXISTS (SELECT 'x' FROM dbo.MedisinskKode mk WHERE mk.EpisodeOid=e.Oid AND mk.MedisinskKodeverk='{MedisinskKodeverkNcmp}')
    e.InnDatoTid BETWEEN '{FraDatotid}' AND '{TilDatotid}'
GROUP BY mk.KodeVerdi");
                }
            }

            public static IEnumerable AntallEpisoderFordeltPåProsedyrekode
            {
                // ReSharper disable once UnusedMember.Local
                get
                {
                    yield return new TestCaseData(
                        nameof(AntallEpisoderFordeltPåProsedyrekode),
                        new List<FieldId>{FieldId.AntallEpisoder, FieldId.Prosedyrekode},
                        new CombinationFilter
                        {
                            LogicalOperator = LogicalOperator.And,
                            Filters = new List<Filter>
                            {
                                new DateTimeFilter(FieldId.InnDatoTid)
                                {
                                    FieldOperator = FieldOperator.Between,
                                    FilterValues = { FraDatotid, TilDatotid},
                                }
                            }
                        },
                        $@"
SELECT 
    COUNT (DISTINCT (e.Oid)), 
    prosedyrekode.KodeVerdi
FROM 
    dbo.Episode e
    LEFT JOIN dbo.MedisinskKode prosedyrekode ON prosedyrekode.EpisodeOid=e.Oid
WHERE 
    e.InnDatoTid BETWEEN '{FraDatotid}' AND '{TilDatotid}'
GROUP BY prosedyrekode.KodeVerdi
");
                }
            }

            public static IEnumerable AntallEpisoderFordeltPåNcmpKode
            {
                // ReSharper disable once UnusedMember.Local
                get
                {
                    yield return new TestCaseData(
                        nameof(AntallEpisoderFordeltPåNcmpKode),
                        new List<FieldId>{FieldId.AntallEpisoder, FieldId.KodeVerdi},
                        new CombinationFilter
                        {
                            LogicalOperator = LogicalOperator.And,
                            Filters = new List<Filter>
                            {
                                new StringFilter(FieldId.MedisinskKodeverk)
                                {
                                    FieldOperator = FieldOperator.Equals,
                                    FilterValues = { "M"},
                                },
                                new StringFilter(FieldId.KodeVerdi)
                                {
                                    FieldOperator = FieldOperator.StartsWith, 
                                    FilterValues = { NcmpKode },
                                },
                                new DateTimeFilter(FieldId.InnDatoTid)
                                {
                                    FieldOperator = FieldOperator.Between,
                                    FilterValues = { FraDatotid, TilDatotid},
                                }
                            }
                        },
                        $@"
SELECT 
    COUNT (DISTINCT (e.Oid)), 
    mk.KodeVerdi
FROM 
    Episode e
    LEFT JOIN MedisinskKode mk ON mk.EpisodeOid=e.Oid
WHERE 
    e.InnDatoTid BETWEEN '{FraDatotid}' AND '{TilDatotid}'
GROUP BY mk.KodeVerdi");
                }
            }

            public static IEnumerable AntallEpisoderMedAngittSærKodeFordeltPåSærkodeOgNcmpKode
            {
                // ReSharper disable once UnusedMember.Local
                get
                {
                    yield return new TestCaseData(
                        nameof(AntallEpisoderMedAngittSærKodeFordeltPåSærkodeOgNcmpKode),
                        new List<FieldId> {FieldId.AntallEpisoder, FieldId.NcmpKodeVerdi, FieldId.NcspKodeVerdi},
                        new CombinationFilter
                        {
                            LogicalOperator = LogicalOperator.And,
                            Filters = new List<Filter>
                            {
                                new StringFilter(FieldId.MedisinskKodeverk)
                                {
                                    FieldOperator = FieldOperator.Equals,
                                    FilterValues = {"M"},
                                },
                                new StringFilter(FieldId.KodeVerdi)
                                {
                                    FieldOperator = FieldOperator.StartsWith,
                                    FilterValues = {NcmpKode},
                                },
                                new DateTimeFilter(FieldId.InnDatoTid)
                                {
                                    FieldOperator = FieldOperator.Between,
                                    FilterValues = {FraDatotid, TilDatotid},
                                }
                            }
                        },
                        $@"
SELECT 
    COUNT(DISTINCT e.Oid), 
    mk1.Kodeverdi [NCMP], 
    mk2.KodeVerdi [NCSP]
FROM 
    episode e
LEFT JOIN MedisinskKode mk1 ON mk1.EpisodeOid=e.Oid
LEFT JOIN MedisinskKode mk2 ON mk2.EpisodeOid=e.Oid
WHERE
    mk1.MedisinskKodeverk='{MedisinskKodeverkNcmp}' AND
    mk2.MedisinskKodeverk='{MedisinskKodeverkSærkode}' AND
    mk2.KodeVerdi = '{Særkode}'
GROUP BY
    mk1.Kodeverdi, mk2.Kodeverdi");
                }
            }

            public static IEnumerable AntallEpisoderMedAngittSærkodeFordeltPåOmsorgsnivå
            {
                // ReSharper disable once UnusedMember.Local
                get
                {
                    yield return new TestCaseData(
                        nameof(AntallEpisoderMedAngittSærkodeFordeltPåOmsorgsnivå),
                        new List<FieldId> {FieldId.AntallEpisoder, FieldId.NcmpKodeVerdi, FieldId.NcspKodeVerdi},
                        new CombinationFilter
                        {
                            LogicalOperator = LogicalOperator.And,
                            Filters = new List<Filter>
                            {
                                new StringFilter(FieldId.MedisinskKodeverk)
                                {
                                    FieldOperator = FieldOperator.Equals,
                                    FilterValues = {"M"},
                                },
                                new StringFilter(FieldId.KodeVerdi)
                                {
                                    FieldOperator = FieldOperator.StartsWith,
                                    FilterValues = {"WBG"},
                                },
                                new DateTimeFilter(FieldId.InnDatoTid)
                                {
                                    FieldOperator = FieldOperator.Between,
                                    FilterValues = {FraDatotid, TilDatotid},
                                }
                            }
                        },
                        $@"
SELECT 
	e.OmsorgNivå, COUNT(DISTINCT e.Oid)
FROM 
	Episode e
WHERE
	EXISTS(SELECT 'x' FROM MedisinskKode mk WHERE mk.EpisodeOid=e.Oid AND mk.MedisinskKodeverk='{MedisinskKodeverkSærkode}' AND mk.KodeVerdi LIKE '{Særkode}%')
GROUP BY e.OmsorgNivå
");
                }
            }

            public static IEnumerable AntallEpisoderMedAngittSærkodeFordeltPåOmsorgsnivåOgSærkode
            {
                // ReSharper disable once UnusedMember.Local
                get
                {
                    yield return new TestCaseData(
                        nameof(AntallEpisoderMedAngittSærkodeFordeltPåOmsorgsnivåOgSærkode),
                        new List<FieldId> {FieldId.AntallEpisoder, FieldId.NcmpKodeVerdi, FieldId.NcspKodeVerdi},
                        new CombinationFilter
                        {
                            LogicalOperator = LogicalOperator.And,
                            Filters = new List<Filter>
                            {
                                new StringFilter(FieldId.MedisinskKodeverk)
                                {
                                    FieldOperator = FieldOperator.Equals,
                                    FilterValues = {"M"},
                                },
                                new StringFilter(FieldId.KodeVerdi)
                                {
                                    FieldOperator = FieldOperator.StartsWith,
                                    FilterValues = {"WBG"},
                                },
                                new DateTimeFilter(FieldId.InnDatoTid)
                                {
                                    FieldOperator = FieldOperator.Between,
                                    FilterValues = {FraDatotid, TilDatotid},
                                }
                            }
                        },
                        $@"
SELECT 
    e.OmsorgNivå, 
    mk.KodeVerdi, COUNT(DISTINCT e.Oid) [AntallEpisoder]
FROM 
	Episode e
	LEFT JOIN MedisinskKode mk ON mk.EpisodeOid=e.Oid
WHERE
	mk.MedisinskKodeverk='{MedisinskKodeverkSærkode}' AND mk.KodeVerdi like '{Særkode}%'
GROUP BY e.OmsorgNivå, mk.KodeVerdi
");
                }
            }

            public static IEnumerable AntallEpisoderMedAngittSærkodeOgAngittNcmpKodeFordeltPåOmsorgsnivåOgSærkode
            {
                // ReSharper disable once UnusedMember.Local
                get
                {
                    yield return new TestCaseData(
                        nameof(AntallEpisoderMedAngittSærkodeOgAngittNcmpKodeFordeltPåOmsorgsnivåOgSærkode),
                        new List<FieldId> {FieldId.AntallEpisoder, FieldId.NcmpKodeVerdi, FieldId.NcspKodeVerdi},
                        new CombinationFilter
                        {
                            LogicalOperator = LogicalOperator.And,
                            Filters = new List<Filter>
                            {
                                new StringFilter(FieldId.MedisinskKodeverk)
                                {
                                    FieldOperator = FieldOperator.Equals,
                                    FilterValues = {MedisinskKodeverkNcmp},
                                },
                                new StringFilter(FieldId.KodeVerdi)
                                {
                                    FieldOperator = FieldOperator.StartsWith,
                                    FilterValues = {NcmpKode},
                                },
                                new DateTimeFilter(FieldId.InnDatoTid)
                                {
                                    FieldOperator = FieldOperator.Between,
                                    FilterValues = {FraDatotid, TilDatotid},
                                }
                            }
                        },
                        $@"
SELECT e.OmsorgNivå, mk.KodeVerdi, COUNT(DISTINCT e.Oid) [AntallEpisoder]
FROM 
	Episode e
	LEFT JOIN MedisinskKode mk ON mk.EpisodeOid=e.Oid
WHERE
	mk.MedisinskKodeverk='{MedisinskKodeverkSærkode}' AND mk.KodeVerdi like '{Særkode}%' AND
	EXISTS (SELECT 'x' FROM MedisinskKode mk WHERE mk.EpisodeOid=e.Oid AND mk.MedisinskKodeverk='{MedisinskKodeverkNcmp}' AND mk.KodeVerdi like '{NcmpKode}%')
GROUP BY e.OmsorgNivå, mk.KodeVerdi
");
                }
            }

            public static IEnumerable AntallEpisoderMedAngittProsedyrekodeFordeltPåOmsorgsnivå
            {
                // ReSharper disable once UnusedMember.Local
                get
                {
                    yield return new TestCaseData(
                        nameof(AntallEpisoderMedAngittProsedyrekodeFordeltPåOmsorgsnivå),
                        new List<FieldId> {FieldId.AntallEpisoder, FieldId.NcmpKodeVerdi, FieldId.NcspKodeVerdi},
                        new CombinationFilter
                        {
                            LogicalOperator = LogicalOperator.And,
                            Filters = new List<Filter>
                            {
                                new StringFilter(FieldId.MedisinskKodeverk)
                                {
                                    FieldOperator = FieldOperator.Equals,
                                    FilterValues = {MedisinskKodeverkNcmp},
                                },
                                new StringFilter(FieldId.KodeVerdi)
                                {
                                    FieldOperator = FieldOperator.StartsWith,
                                    FilterValues = {NcmpKode},
                                },
                                new DateTimeFilter(FieldId.InnDatoTid)
                                {
                                    FieldOperator = FieldOperator.Between,
                                    FilterValues = {FraDatotid, TilDatotid},
                                }
                            }
                        },
                        $@"
SELECT 
	e.OmsorgNivå, COUNT(DISTINCT e.Oid)
FROM 
	dbo.Episode e
WHERE
	EXISTS(SELECT 'x' FROM dbo.MedisinskKode mk WHERE mk.EpisodeOid=e.Oid AND mk.KodeVerdi LIKE '{NcmpKode}%')
GROUP BY e.OmsorgNivå");
                }
            }

            public static IEnumerable AntallEpisoderFordeltPåNcmpKodeOgSærkode
            {
                // ReSharper disable once UnusedMember.Local
                get
                {
                    yield return new TestCaseData(
                        nameof(AntallEpisoderFordeltPåNcmpKodeOgSærkode),
                        new List<FieldId> {FieldId.AntallEpisoder, FieldId.NcmpKodeVerdi, FieldId.NcspKodeVerdi},
                        new DateTimeFilter(FieldId.InnDatoTid)
                        {
                            FieldOperator = FieldOperator.Between,
                            FilterValues = {FraDatotid, TilDatotid},
                        },
                        $@"
SELECT 
	COUNT(DISTINCT e.Oid), mk1.KodeVerdi, mk2.KodeVerdi
FROM 
	dbo.Episode e
    LEFT JOIN MedisinskKode mk1 ON mk1.EpisodeOid=e.Oid AND mk1.{FieldId.MedisinskKodeverk}='{MedisinskKodeverkNcmp}'
    LEFT JOIN MedisinskKode mk2 ON mk1.EpisodeOid=e.Oid AND mk2.{FieldId.MedisinskKodeverk}='{MedisinskKodeverkSærkode}'
WHERE
    e.InnDatoTid BETWEEN '{FraDatotid}' AND '{TilDatotid}'
GROUP BY e.OmsorgNivå");
                }
            }
        }

        #endregion
    }
}
