﻿using System;
using System.Collections;
using System.Collections.Generic;
using Models.Enums;
using Models.Filters;
using Models.Interfaces;
using Models.QueryBuilders;
using NUnit.Framework;

namespace ModelsUnitTests
{
    [TestFixture]
    public class FilterUnitTests
    {
        #region Class: MockQueryBuilder

        private class MockQueryBuilder : IQueryBuilder
        {
            public IList<FieldId> Fields { get; } = new List<FieldId>();
            public Filter Filter { get; set; }
            public IDictionary<FieldId, FieldInfo> FieldInfos { get; } = new Dictionary<FieldId, FieldInfo>();
            public IDictionary<FieldId, TableInfo> FieldTableInfoMap { get; } = new Dictionary<FieldId, TableInfo>();

            public string Sql
            {
                get { throw new NotImplementedException(); }
            }
        }

            #endregion

        [Test]
        [TestCaseSource(typeof(TestInput), nameof(TestInput.SimpleStringFilter))]
        [TestCaseSource(typeof(TestInput), nameof(TestInput.CombinedStringFilter))]
        public void FiltersReturnsExpectedDescription(string caseDescription, Filter filter, string tableAlias, FieldId fieldId, FieldInfo fieldInfo, string expectedDescription, string expectedSql)
        {
            var queryBuilder = new MockQueryBuilder
            {
                FieldInfos =
                {
                    {
                        fieldId, fieldInfo
                    }
                },
                FieldTableInfoMap =
                {
                    {
                        fieldId, new TableInfo(tableAlias)
                    }
                }
            };
            filter.QueryBuilder = queryBuilder;

            Assert.That(filter.Description, Is.EqualTo(expectedDescription), caseDescription);
            var sql = filter.GetSql();
            Assert.That(sql, Is.EqualTo(expectedSql), caseDescription);
        }

        #region Class: TestInput

        private static class TestInput
        {
            private const string Prefix9 = "9";
            private const string N10 = "N10";
            private const string N11 = "N11";

            public static IEnumerable SimpleStringFilter
            {
                get
                {
                    var tableAlias = "gurba";
                    var fieldId = FieldId.Hovedtilstand;
                    var fieldInfo = new FieldInfo {TableColumn = new TableColumn(TableId.Episode, ColumnId.Hovedtilstand)};

                    yield return new TestCaseData(
                        nameof(SimpleStringFilter),
                        new StringFilter(fieldId)
                        {
                            FieldOperator = FieldOperator.StartsWith,
                            FilterValues = {N10, N11},
                        },
                        tableAlias,
                        fieldId,
                        fieldInfo,
                        $@"{FieldOperator.StartsWith} ({N10}, {N11})",
                        $@"({tableAlias}.{fieldId} LIKE '{N10}%' OR {tableAlias}.{fieldId} LIKE '{N11}%')"
                    );
                }
            }

            public static IEnumerable CombinedStringFilter
            {
                get
                {
                    var tableAlias = "harry";
                    var fieldId = FieldId.Hovedtilstand;
                    var fieldInfo = new FieldInfo {TableColumn = new TableColumn(TableId.MedisinskKode, ColumnId.Hovedtilstand)};

                    var logicalOperator = LogicalOperator.And;

                    yield return new TestCaseData(
                        nameof(CombinedStringFilter),
                        new CombinationFilter
                        {
                            LogicalOperator = logicalOperator,
                            Filters = new List<Filter>
                            {
                                new StringFilter(fieldId)
                                {
                                    FieldOperator = FieldOperator.StartsWith,
                                    FilterValues = {N10, N11},
                                },
                                new StringFilter(fieldId)
                                {
                                    FieldOperator = FieldOperator.EndsWith,
                                    FilterValues = {Prefix9}
                                }
                            }
                        },
                        tableAlias,
                        fieldId,
                        fieldInfo,
                        $@"({FieldOperator.StartsWith} ({N10}, {N11}) {logicalOperator} {FieldOperator.EndsWith} ({Prefix9}))",
                        $@"(({tableAlias}.{fieldId} LIKE '{N10}%' OR {tableAlias}.{fieldId} LIKE '{N11}%') {logicalOperator} {tableAlias}.{fieldId} LIKE '%{Prefix9}')"
                    );
                }
            }
        }

        #endregion
    }
}
